package com.phapli.android_engineering_challenge.util;

import android.content.Context;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.HashMap;
import java.util.Map;

public class Utils {
    private static final String TAG = Utils.class.getSimpleName();
    private static Map<String, String> currencyMap = new HashMap<>();
    private static DecimalFormat currencyFormat = (DecimalFormat) NumberFormat.getCurrencyInstance();

    static {
        currencyMap.put("USD", "$");
        currencyMap.put("GBP", "£");
        currencyMap.put("VND", "₫");
        currencyMap.put("CAD", "C$");
        currencyMap.put("AUD", "A$");
        DecimalFormatSymbols symbols = currencyFormat.getDecimalFormatSymbols();
        symbols.setCurrencySymbol(""); // Don't use null.
        currencyFormat.setDecimalFormatSymbols(symbols);
    }

    public static String roundPrice(float price) {
        if(price < 0){
            return "-";
        }
//        return String.format("%.02f", price);
        return currencyFormat.format(price);
    }

    public static String readFileFromAsset(Context context, String fileName) {
        String data = null;
        try {
            if (context != null) {
                InputStream is = context.getAssets().open(fileName);
                int size = is.available();
                byte[] buffer = new byte[size];
                is.read(buffer);
                is.close();
                data = new String(buffer, "UTF-8");
            }
        } catch (IOException ex) {
            Log.e(TAG, "readFileFromAsset: ", ex);
            return null;
        }
        return data;
    }

    public static float parseFloat(String value, float defaultValue) {
        try {
            return Float.valueOf(value);
        } catch (Exception e) {
            Log.e(TAG, "parseFloat: ", e);
        }
        return defaultValue;
    }

    public static String getCurrencySymbol(String currency) {
        String symbol = currencyMap.get(currency);
        if (symbol == null) {
            symbol = currency;
        }
        return symbol;
    }

}
