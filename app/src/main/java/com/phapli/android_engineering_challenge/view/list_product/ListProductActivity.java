package com.phapli.android_engineering_challenge.view.list_product;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.phapli.android_engineering_challenge.R;
import com.phapli.android_engineering_challenge.datastore.CurrencyRate;
import com.phapli.android_engineering_challenge.datastore.ProductStore;
import com.phapli.android_engineering_challenge.model.Product;
import com.phapli.android_engineering_challenge.model.ProductRepository;
import com.phapli.android_engineering_challenge.model.Rate;
import com.phapli.android_engineering_challenge.model.RateRepository;
import com.phapli.android_engineering_challenge.model.impl.local.ProductRepositoryLocal;
import com.phapli.android_engineering_challenge.model.impl.local.RateRepositoryLocal;
import com.phapli.android_engineering_challenge.view.detail_product.DetailProductActivity;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ListProductActivity extends AppCompatActivity {

    public static final String EXTRA_PRODUCT_SKU = "SKU";
    private static final String TAG = ListProductActivity.class.getSimpleName();
    private ListView lvProduct;
    private ListProductAdapter productAdapter;
    private List<Product> products;
    CurrencyRate currencyRate = CurrencyRate.getInstance();
    ProductStore productStore = ProductStore.getInstance();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_product);
        setTitle(R.string.list_product_activity_title);

        products = new ArrayList<>();
        productAdapter = new ListProductAdapter(this);
        lvProduct = (ListView) findViewById(R.id.list_view_product);
        lvProduct.setAdapter(productAdapter);

        lvProduct.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(ListProductActivity.this, DetailProductActivity.class);
                intent.putExtra(EXTRA_PRODUCT_SKU, products.get(i).getSku());
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        new FetchDataTask().execute();
    }

    private void fetchData() {
        RateRepository rateRepository = new RateRepositoryLocal(this, "rates.json");
        Map<String, Rate> rateMap = rateRepository.fetchAll();
        Log.d(TAG, "fetchData: rateMap size" + rateMap.size());
        currencyRate.updateExchangeRate(rateMap);

        ProductRepository productRepository = new ProductRepositoryLocal(this, "transactions.json");
        final Map<String, Product> productMap = productRepository.fetchAll();
        productStore.updateProducts(productMap);
        products = new ArrayList<>(productMap.values());
        Log.d(TAG, "fetchData: products size" + products.size());
        runOnUiThread(new Runnable() {
            public void run() {
                productAdapter.updateProducts(products);
            }
        });
    }

    class FetchDataTask extends AsyncTask<Object, Object, Void> {
        ProgressDialog pd;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(ListProductActivity.this);
            pd.setMessage(getString(R.string.init_data));
            pd.show();
        }

        @Override
        protected Void doInBackground(Object... params) {
            fetchData();
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            if (pd != null)
            {
                pd.dismiss();
            }
        }
    }


}
