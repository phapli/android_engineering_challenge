package com.phapli.android_engineering_challenge.view.list_product;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.phapli.android_engineering_challenge.R;
import com.phapli.android_engineering_challenge.model.Product;

import java.util.Collections;
import java.util.List;

public class ListProductAdapter extends BaseAdapter {

    private List<Product> products = Collections.emptyList();

    private final Context context;

    // the context is needed to inflate views in getView()
    public ListProductAdapter(Context context) {
        this.context = context;
    }

    public void updateProducts(List<Product> products) {
        this.products = products;
        notifyDataSetChanged();
    }

    public void clear() {
        products.clear();
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return products.size();
    }

    // getItem(int) in Adapter returns Object but we can override
    // it to BananaPhone thanks to Java return type covariance
    @Override
    public Product getItem(int position) {
        return products.get(position);
    }

    // getItemId() is often useless, I think this should be the default
    // implementation in BaseAdapter
    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Let's look at that later
        TextView tv_sku;
        TextView tv_transaction_count;
        if (convertView == null) {
            convertView = LayoutInflater.from(context)
                    .inflate(R.layout.item_list_product, parent, false);
            tv_sku = (TextView) convertView.findViewById(R.id.text_view_product_sku);
            tv_transaction_count = (TextView) convertView.findViewById(R.id.text_view_transaction_count);
            convertView.setTag(new ViewHolder(tv_sku, tv_transaction_count));
        } else {
            ViewHolder viewHolder = (ViewHolder) convertView.getTag();
            tv_sku = viewHolder.sku;
            tv_transaction_count = viewHolder.transaction_count;
        }
        Product product = getItem(position);
        tv_sku.setText(product.getSku());
        tv_transaction_count.setText(String.valueOf(product.getTransactionCount()) + context.getString(R.string.transactions));

        return convertView;
    }

    private static class ViewHolder {
        public final TextView sku;
        public final TextView transaction_count;

        public ViewHolder(TextView sku, TextView transaction_count) {
            this.sku = sku;
            this.transaction_count = transaction_count;
        }
    }

}


