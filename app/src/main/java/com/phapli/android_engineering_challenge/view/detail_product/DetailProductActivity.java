package com.phapli.android_engineering_challenge.view.detail_product;

import android.app.ActionBar;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ListView;
import android.widget.TextView;

import com.phapli.android_engineering_challenge.R;
import com.phapli.android_engineering_challenge.datastore.ProductStore;
import com.phapli.android_engineering_challenge.model.Product;
import com.phapli.android_engineering_challenge.model.Transaction;
import com.phapli.android_engineering_challenge.view.list_product.ListProductActivity;

import java.util.ArrayList;
import java.util.List;

public class DetailProductActivity extends AppCompatActivity {

    private ListView lvTransaction;
    private DetailProductAdapter detailProductAdapter;
    private TextView tvTotalPrice;
    private Product product;
    private ProductStore productStore = ProductStore.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_product);
        tvTotalPrice = (TextView) findViewById(R.id.text_view_total_price);
        lvTransaction = (ListView) findViewById(R.id.list_view_transaction);

        String sku = getIntent().getStringExtra(ListProductActivity.EXTRA_PRODUCT_SKU);
        setTitle(getString(R.string.detail_product_activity_title) + sku);
        product = productStore.getProduct(sku);
        detailProductAdapter = new DetailProductAdapter(this);
        lvTransaction.setAdapter(detailProductAdapter);


    }

    @Override
    protected void onResume() {
        super.onResume();
        if (product != null) {
            detailProductAdapter.updateTransactions(product.getTransactions());
            tvTotalPrice.setText(getString(R.string.total) + product.getTotalPrice());
        }
    }
}
