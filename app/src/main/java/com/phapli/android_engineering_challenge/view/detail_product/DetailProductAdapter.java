package com.phapli.android_engineering_challenge.view.detail_product;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.phapli.android_engineering_challenge.R;
import com.phapli.android_engineering_challenge.model.Transaction;

import java.util.ArrayList;
import java.util.List;

public class DetailProductAdapter extends BaseAdapter {

    private List<Transaction> transactions = new ArrayList<>();

    private final Context context;

    // the context is needed to inflate views in getView()
    public DetailProductAdapter(Context context) {
        this.context = context;
    }

    public void updateTransactions(List<Transaction> transactions) {
        this.transactions = transactions;
        notifyDataSetChanged();
    }

    public void clear() {
        transactions.clear();
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return transactions.size();
    }

    // getItem(int) in Adapter returns Object but we can override
    // it to BananaPhone thanks to Java return type covariance
    @Override
    public Transaction getItem(int position) {
        return transactions.get(position);
    }

    // getItemId() is often useless, I think this should be the default
    // implementation in BaseAdapter
    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Let's look at that later
        TextView tv_origin_price;
        TextView tv_new_price;
        if (convertView == null) {
            convertView = LayoutInflater.from(context)
                    .inflate(R.layout.item_detail_product, parent, false);
            tv_origin_price = (TextView) convertView.findViewById(R.id.text_view_origin_price);
            tv_new_price = (TextView) convertView.findViewById(R.id.text_view_converted_price);
            convertView.setTag(new ViewHolder(tv_origin_price, tv_new_price));
        } else {
            ViewHolder viewHolder = (ViewHolder) convertView.getTag();
            tv_origin_price = viewHolder.origin_price;
            tv_new_price = viewHolder.new_price;
        }
        Transaction transaction = getItem(position);
        tv_origin_price.setText(transaction.getOriginPrice());
        tv_new_price.setText(transaction.getGBPPrice());

        return convertView;
    }

    private static class ViewHolder {
        public final TextView origin_price;
        public final TextView new_price;

        public ViewHolder(TextView origin_price, TextView new_price) {
            this.origin_price = origin_price;
            this.new_price = new_price;
        }
    }

}


