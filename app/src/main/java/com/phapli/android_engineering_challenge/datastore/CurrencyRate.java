package com.phapli.android_engineering_challenge.datastore;

import com.phapli.android_engineering_challenge.model.Rate;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;

public class CurrencyRate {
    private static CurrencyRate instance;
    private List<String> currencies = new ArrayList<>();
    private float adjacency_matrix[][] = new float[0][0];

    public static CurrencyRate getInstance() {
        if (instance == null) {
            // Create the instance
            instance = new CurrencyRate();
        }
        return instance;
    }

    private CurrencyRate() {
        // Constructor hidden because this is a singleton
    }

    public void updateExchangeRate(Map<String, Rate> rateMap) {
        List<Rate> rates = new ArrayList<>(rateMap.values());
        Set<String> currencySet = new HashSet<>();
        for (Rate rate : rates) {
            currencySet.add(rate.getFrom());
            currencySet.add(rate.getTo());
        }
        currencies = new ArrayList<>(currencySet);
        adjacency_matrix = new float[currencySet.size()][currencySet.size()];
        for (int i = 0; i < currencySet.size(); i++)
            for (int j = 0; j < currencySet.size(); j++)
                if (i == j)
                    adjacency_matrix[i][j] = 1;
                else
                    adjacency_matrix[i][j] = 0;
        for (Rate rate : rates) {
            adjacency_matrix[currencies.indexOf(rate.getFrom())][currencies.indexOf(rate.getTo())] = rate.getRate();
        }
    }

    public float convert(String from, String to) {
        int fromIndex = currencies.indexOf(from);
        int toIndex = currencies.indexOf(to);
        if (fromIndex < 0 || toIndex < 0) {
            return -1;
        } else {
            return convert(fromIndex, toIndex);
        }
    }

    private float convert(int source, int destination) {
        if (adjacency_matrix[source][destination] > 0) {
            return adjacency_matrix[source][destination];
        }

        Queue<TempNode> queue = new LinkedList<>();
        int number_of_nodes = adjacency_matrix[source].length;

        int[] visited = new int[number_of_nodes];
        int i;
        TempNode element;

        visited[source] = 1;
        queue.add(new TempNode(source, 1));

        while (!queue.isEmpty()) {
            element = queue.remove();
            i = 0;
            while (i < number_of_nodes) {
                if (adjacency_matrix[element.getNode()][i] > 0 && visited[i] == 0) {
                    float rate = adjacency_matrix[element.getNode()][i] * element.getRate();
                    queue.add(new TempNode(i, rate));
                    visited[i] = 1;
                    if (i == destination) {
                        return rate;
                    }
                }
                i++;
            }
        }
        return -1;
    }

    private class TempNode {
        private int node;
        private float rate;

        public TempNode(int node, float rate) {
            this.node = node;
            this.rate = rate;
        }

        public int getNode() {
            return node;
        }

        public float getRate() {
            return rate;
        }
    }

}
