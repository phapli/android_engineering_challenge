package com.phapli.android_engineering_challenge.datastore;

import com.phapli.android_engineering_challenge.model.Product;
import com.phapli.android_engineering_challenge.model.Rate;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;

public class ProductStore {
    private static ProductStore instance;
    private Map<String, Product> products = new HashMap<>();

    public static ProductStore getInstance() {
        if (instance == null) {
            // Create the instance
            instance = new ProductStore();
        }
        return instance;
    }

    private ProductStore() {
        // Constructor hidden because this is a singleton
    }

    public void updateProducts(Map<String, Product> products) {
        this.products = products;
    }

    public Product getProduct(String sku) {
        return products.get(sku);
    }
}
