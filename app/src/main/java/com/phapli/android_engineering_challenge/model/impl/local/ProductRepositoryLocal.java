package com.phapli.android_engineering_challenge.model.impl.local;

import android.content.Context;
import android.util.Log;

import com.phapli.android_engineering_challenge.model.Product;
import com.phapli.android_engineering_challenge.model.ProductRepository;
import com.phapli.android_engineering_challenge.model.Transaction;
import com.phapli.android_engineering_challenge.util.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ProductRepositoryLocal implements ProductRepository {

    private static final String TAG = ProductRepositoryLocal.class.getSimpleName();
    private final Context context;
    private final String fileName;

    public ProductRepositoryLocal(Context context, String fileName) {
        this.context = context;
        this.fileName = fileName;
    }

    @Override
    public Map<String, Product> fetchAll() {
        Map<String, Product> productMap = new HashMap<>();
        try {
            String data = Utils.readFileFromAsset(context, fileName);
            if (data != null) {
                JSONArray transactionJsonArray = new JSONArray(data);
                for (int i = 0; i < transactionJsonArray.length(); i++) {
                    JSONObject transactionJsonObject = transactionJsonArray.getJSONObject(i);
                    String sku = transactionJsonObject.getString("sku");
                    String currency = transactionJsonObject.getString("currency");
                    String amountString = transactionJsonObject.getString("amount");
                    float amountValue = Utils.parseFloat(amountString, 0f);
                    Transaction transaction = new Transaction(sku, amountValue, currency);
                    Product product = productMap.get(sku);
                    if (product == null) {
                        product = new Product(sku);
                    }
                    product.addTransaction(transaction);
                    productMap.put(sku, product);
                }
            }
        } catch (JSONException e) {
            Log.e(TAG, "fetchAll: ", e);
        }
        return productMap;
    }

    @Override
    public Product findById(String id) {
        return null;
    }

    @Override
    public boolean save(Product product) {
        return false;
    }
}
