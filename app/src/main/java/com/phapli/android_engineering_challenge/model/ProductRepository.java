package com.phapli.android_engineering_challenge.model;

import java.util.List;
import java.util.Map;

public interface ProductRepository {
    Map<String, Product> fetchAll();

    Product findById(String id);

    boolean save(Product product);
}
