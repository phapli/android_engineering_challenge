package com.phapli.android_engineering_challenge.model;

import java.util.List;
import java.util.Map;

public interface TransactionRepository {
    Map<String, Rate> fetchAll();

    List<Transaction> findById(String id);

    boolean save(Transaction transaction);
}
