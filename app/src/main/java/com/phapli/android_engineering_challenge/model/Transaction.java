package com.phapli.android_engineering_challenge.model;

import com.phapli.android_engineering_challenge.datastore.CurrencyRate;
import com.phapli.android_engineering_challenge.util.Utils;

public class Transaction {
    private String sku;
    private float amount;
    private String currency;

    public Transaction(String sku, float amount, String currency) {
        this.sku = sku;
        this.amount = amount;
        this.currency = currency;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public float getAmount() {
        return amount;
    }

    public void setAmount(float amount) {
        this.amount = amount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getOriginPrice() {
        return Utils.getCurrencySymbol(currency) + Utils.roundPrice(amount);
    }

    public float getGBPAmount(){
        return amount * CurrencyRate.getInstance().convert(currency, "GBP");
    }

    public String getGBPPrice() {
        return Utils.getCurrencySymbol("GBP") + Utils.roundPrice(getGBPAmount());
    }

}
