package com.phapli.android_engineering_challenge.model.impl.local;

import android.content.Context;
import android.util.Log;

import com.phapli.android_engineering_challenge.model.Rate;
import com.phapli.android_engineering_challenge.model.RateRepository;
import com.phapli.android_engineering_challenge.util.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class RateRepositoryLocal implements RateRepository {

    private static final String TAG = RateRepository.class.getSimpleName();
    private final Context context;
    private final String fileName;

    public RateRepositoryLocal(Context context, String fileName) {
        this.context = context;
        this.fileName = fileName;
    }

    @Override
    public Map<String, Rate> fetchAll() {
        Map<String, Rate> rateMap = new HashMap<>();
        try {
            String data = Utils.readFileFromAsset(context, fileName);
            if(data !=null) {
                JSONArray rateJsonArray = new JSONArray(data);

                for (int i = 0; i < rateJsonArray.length(); i++) {
                    JSONObject rateJsonObject = rateJsonArray.getJSONObject(i);
                    String from = rateJsonObject.getString("from");
                    String to = rateJsonObject.getString("to");
                    String rateString = rateJsonObject.getString("rate");
                    float rateValue = Utils.parseFloat(rateString, 0f);
                    Rate rate = new Rate(from, to, rateValue);
                    rateMap.put(rate.getId(), rate);
                }
            }
        } catch (JSONException e) {
            Log.e(TAG, "fetchAll: ", e);
        }
        return rateMap;
    }

    @Override
    public Rate findById(String id) {
        return null;
    }

    @Override
    public boolean save(Rate rate) {
        return false;
    }
}
