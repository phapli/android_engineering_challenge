package com.phapli.android_engineering_challenge.model;

import com.phapli.android_engineering_challenge.util.Utils;

import java.util.ArrayList;
import java.util.List;

public class Product {
    private String sku;
    private long transactionCount = 0;
    private List<Transaction> transactions = new ArrayList<>();

    public Product(String sku) {
        this.sku = sku;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public long getTransactionCount() {
        return transactionCount;
    }

    public void setTransactionCount(long transactionCount) {
        this.transactionCount = transactionCount;
    }

    public void addTransaction(Transaction transaction) {
        transactions.add(transaction);
        transactionCount = transactions.size();
    }

    public List<Transaction> getTransactions() {
        return transactions;
    }

    public float getTotalAmount() {
        float totalAmount = 0f;
        for (Transaction transaction: transactions){
            totalAmount += transaction.getGBPAmount();
        }
        return totalAmount;
    }

    public String getTotalPrice() {
        return Utils.getCurrencySymbol("GBP") + Utils.roundPrice(getTotalAmount());
    }
}
