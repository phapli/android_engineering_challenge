package com.phapli.android_engineering_challenge.model;

import java.util.Map;

public interface RateRepository {
    Map<String, Rate> fetchAll();

    Rate findById(String id);

    boolean save(Rate rate);
}
