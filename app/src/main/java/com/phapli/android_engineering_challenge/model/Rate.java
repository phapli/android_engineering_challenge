package com.phapli.android_engineering_challenge.model;

public class Rate {
    private String id;
    private String from;
    private String to;
    private float rate;

    public Rate(String from, String to, float rate) {
        this.id = from + "_" + to;
        this.from = from;
        this.to = to;
        this.rate = rate;
    }

    public String getId() {
        if (id == null) {
            this.id = this.from + "_" + this.to;
        }
        return id;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public float getRate() {
        return rate;
    }

    public void setRate(float rate) {
        this.rate = rate;
    }
}
